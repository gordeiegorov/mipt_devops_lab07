ARG JAVA_VERSION=openjdk11
ARG ALPINE_VERSION=3.14

# SYSTEM
FROM alpine:$ALPINE_VERSION as system

# Install java
ARG JAVA_VERSION
ENV JAVA_VERSION $JAVA_VERSION

RUN  apk update \
  && apk upgrade \
  && apk add --update coreutils && rm -rf /var/cache/apk/* \
  && apk add --update $JAVA_VERSION \
  && apk add --no-cache nss \
  && rm -rf /var/cache/apk/*

# Install maven
ENV MAVEN_VERSION 3.5.4
ENV MAVEN_HOME /usr/lib/mvn
ENV PATH $MAVEN_HOME/bin:$PATH

RUN wget http://archive.apache.org/dist/maven/maven-3/$MAVEN_VERSION/binaries/apache-maven-$MAVEN_VERSION-bin.tar.gz && \
  tar -zxvf apache-maven-$MAVEN_VERSION-bin.tar.gz && \
  rm apache-maven-$MAVEN_VERSION-bin.tar.gz && \
  mv apache-maven-$MAVEN_VERSION /usr/lib/mvn

WORKDIR /usr/src/app



# BUILD
FROM system AS build

# Install dependencies
COPY React-Spring-App-DevOps/spring-backend/pom.xml .
RUN mvn dependency:resolve

# Build and clean
COPY React-Spring-App-DevOps/spring-backend/src ./src
RUN mvn package -Dmaven.test.skip=true \
 && cp ./target/*.jar main.jar \
 && rm -r src && rm pom.xml



# RUN
FROM alpine:$ALPINE_VERSION

# Install java
ARG JAVA_VERSION
ENV JAVA_VERSION $JAVA_VERSION

RUN  apk update \
  && apk upgrade \
  && apk add --update $JAVA_VERSION \
  && apk add --no-cache nss \
  && apk add --no-cache curl \
  && rm -rf /var/cache/apk/*

# Copy jar
WORKDIR /usr/src/app
COPY --from=build /usr/src/app/main.jar ./main.jar

# Launch as non-sudo user
USER 1001

# Run
ENTRYPOINT ["java"]
CMD ["-jar", "main.jar"]
