# NODE
FROM alpine:3.18 as node_inst


RUN  apk update \
  && apk upgrade \
  && apk add --update nodejs npm \
  && rm -rf /var/cache/apk/*


# BUILD
FROM node_inst AS build

WORKDIR /build

COPY React-Spring-App-DevOps/react-frontend/package.json ./
COPY React-Spring-App-DevOps/react-frontend/package-lock.json ./
RUN npm ci

COPY React-Spring-App-DevOps/react-frontend/public/ public
COPY React-Spring-App-DevOps/react-frontend/src/ src
RUN npm run build





# RUN
FROM alpine:3.18

RUN apk update && \
    apk add --no-cache nginx nginx-mod-http-headers-more

EXPOSE 80

WORKDIR /usr/share/nginx/html/

COPY --from=build /build/build/* /usr/share/nginx/html/

RUN mkdir /usr/share/nginx/html/static && \
    mv /usr/share/nginx/html/css /usr/share/nginx/html/static/ && \
    mv /usr/share/nginx/html/js /usr/share/nginx/html/static/ && \
    mv /usr/share/nginx/html/media /usr/share/nginx/html/static/

# Launch as non-sudo user
RUN chown -R 1001:0 /var/lib/nginx /var/log/nginx /var/run/ && \
    chmod -R g=u /var/lib/nginx /var/log/nginx /var/run/

USER 1001

CMD ["nginx", "-g", "daemon off;"]
