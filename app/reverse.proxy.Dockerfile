FROM alpine:3.18

RUN apk update && \
    apk add --no-cache nginx nginx-mod-http-headers-more

EXPOSE 80

# Launch as non-sudo user
RUN chown -R 1001:0 /var/lib/nginx /var/log/nginx /var/run/ && \
    chmod -R g=u /var/lib/nginx /var/log/nginx /var/run/
USER 1001

CMD ["nginx", "-g", "daemon off;"]
