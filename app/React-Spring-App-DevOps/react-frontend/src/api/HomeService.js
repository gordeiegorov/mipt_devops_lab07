import React from "react";
import axios from "axios";

const HomeService = () => {
  try {
    return axios.get(window.location.origin+":81/api/v1");
  } catch (err) {
    let error = "";
    if (err.response) {
      error += err.response;
    }
    return error;
  }
};

export default HomeService;
