# MIPT_devops_lab07

This project uses file `docker_build_push.yml` from https://gitlab.com/gordeiegorov/mipt_devops_docker_build (main)

In *this* repository the file `docker_build_push.yml` was uploaded for learning purposes only, this copy is not used in any way

Requires 5 variables configured in gitlab ci/cd settings:

- *CI_REGISTRY*=docker.io                    # registry url
- *CI_REGISTRY_NAMESPACE*=gissys             # username for docker.io, otherwise some other namespace
- *CI_REGISTRY_PASSWORD*=[token]             # token or password for accessing dockerhub
- *CI_REGISTRY_USER*=gordeiegorov@gmail.com  # username
- *SSH_PRIVATE_KEY*=[secret key]             # private key for connecting to ssh

and 1 file-type variable:

- *SSH_KNOWN_HOSTS*=[result of "ssh-keyscan 84.201.167.21"]  # known hosts for ssh connection to the server
